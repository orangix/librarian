FROM golang:alpine AS build

WORKDIR /src
RUN apk --no-cache add git ca-certificates
RUN git clone https://codeberg.org/librarian/librarian .

RUN go mod download
RUN CGO_ENABLED=0 go build -o /src/librarian

FROM scratch as bin

WORKDIR /app
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /src/librarian .

EXPOSE 3000

CMD ["/app/librarian"]
