module codeberg.org/librarian/librarian

go 1.16

require (
	github.com/dustin/go-humanize v1.0.0
	github.com/gofiber/fiber/v2 v2.29.0
	github.com/gofiber/template v1.6.25
	github.com/gorilla/feeds v1.1.1
	github.com/hashicorp/go-retryablehttp v0.7.0
	github.com/lucas-clemente/quic-go v0.25.0
	github.com/microcosm-cc/bluemonday v1.0.18
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/spf13/viper v1.10.1
	github.com/tidwall/gjson v1.14.0
	github.com/yuin/goldmark v1.4.10
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
